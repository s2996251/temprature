package com.example.temprature;

import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "temperatureCal", value = "/fahrenheitCal")
public class temperatureCalculatorServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        double celcius = Double.parseDouble(request.getParameter("celcius"));
        double fahrenheit = celcius * (9.0/5.0) + 32;

        PrintWriter out = response.getWriter();
        out.println("<!DOCTYPE HTML>\n" +
                "<!-- Front end for the webBookQuote Servlet. -->\n" +
                "\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <title>Fahrenheit</title>\n" +
                "    <link rel=\"stylesheet\" href=\"styles.css\">\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "</head>\n" +
                "\n" +
                "<body>\n" +
                "<h1>Fahrenheit</h1>\n" +
                "\n" +
                "<p>" + celcius + " celcius is equal to " + fahrenheit + " fahrenheit! </p>" +
                "\n" +
                "</body>\n" +
                "</html>");
    }
}
